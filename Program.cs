﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace encrypt_test
{
    class Program
    {
        static string key = "AAal$ac¡a*105.27";
        // static string keyToken = "alsacia.105*";
        static void Main(string[] args)
        {
            // string toEncrypt = "abc123.";
            // Console.WriteLine(Encrypt(toEncrypt, key));
            // Console.WriteLine(Decrypt("TkEyz/sftFo=", key));
            
            FileStream fResponse = new FileStream("./result.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter write = new StreamWriter(fResponse);
            TextWriter oldOut = Console.Out;

            string response = "TkEyz/sftFo=";

            Console.SetOut(write);
            Console.WriteLine(Decrypt(response, key));
            // Console.WriteLine(Token(response));
            Console.SetOut (oldOut);

            write.Close();
            fResponse.Close();

            Console.WriteLine("Finalizado");
        }

        static string Encrypt(string toEncrypt, string key) {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        static string Decrypt(string toEncrypt, string key) {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toEncrypt);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        static string Token(string strText){
            string str = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            byte[] bytes = Encoding.UTF8.GetBytes(strText);
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(1024))
            {
                try
                {
                    rsa.FromXmlString(str.ToString());
                    return Convert.ToBase64String(rsa.Encrypt(bytes, true));
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
    }
}
